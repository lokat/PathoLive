PathoLive - Description
=======================================

Description
-----------

PathoLive is a real time pathogen diagnostics tool that detects pathogenic 
sequences from metagenomic Illumina sequencing data while they are
produced. This means, a hint towards a diagnosis is provided before the
sequencer is finished. 


Website
-------

The PathoLive project website is https://gitlab.com/SimonHTausch/PathoLive

There you can find the latest version of PathoLive, source code, documentation,
and examples.


Installation
------------
PathoLive doesn't require any installation.  
However, make sure that the following dependencies are installed:

 * HiLive2 (>= v2.0)
 * python3 

HiLive2 can be downloaded at https://gitlab.com/rki_bioinformatics/hilive2.
Please follow the installation instructions provided at the HiLive2 project page.


Usage
-----


#### Default mode
To use PathoLive, please download and unpack all provided files without changing their structure.

To start PathoLive with the default settings, enter:

    ./PathoLive -r 101R,8B,8B,101R -b /path/to/BaseCalls -i /path/to/reference -o /path/to/resultfolder -H /path/to/hilive2 -O 50,101,167,218

Thereby, the ``-r`` parameter specifies the segments of the sequencing run (here: 2x101bp reads with 2x8bp barcodes). ``-O`` specifies the cycles for that analysis output is produced.  
  
PathoLive will produce .BAM files for the specified cycles. Further, for each output cycle, a subdirectory ``output_cycle{CYCLE}`` will be produced, where {CYCLE} is the respective output cycle number. In ``output_cycle{CYCLE}/Collapsible_Tree_with_Amounts/index.html``, you find a browser-accessible visualization of the analysis results of PathoLive.

We recommend to adjust the numbers of threads used by PathoLive with ``-n``. If possible, make use of as many threads as tiles are produced by your sequencer.

Building your own reference database
------------------------------------

We advise you to use the reference database provided by the PathoLive team.  
You find the prebuilt database here: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.2536788.svg)](https://doi.org/10.5281/zenodo.2536788)  
  
Use the ``hilive-build`` executable of HiLive2 to build the index that is required for live analysis.  
  
However, you can also build your own reference database.  
Disclaimer: This requires to download several hundreds of gigabytes of data. 

Make sure to install the following additional dependencies:
 * bowtie2
 * fastq-dump
 * trimmomatic

Build a Bowtie2 index as well as a HiLive index of your reference file. 
Insert the paths to the Bowtie2 index and a result folder and start the bash-script 
    ``../background_definition/download_trim_map_background.bsh``.

Then start the python script ``calculate_overall_background_coverage.py`` with the provided result folder as input folder and replace the file ``prelim_data/background_coverages.pickle`` in your PathoLive-folder by the new one.


License
-------

See the file LICENSE.md for licensing information.


Contact
-------

Please consult the PathoLive project website for questions!

If this does not help, please feel free to consult:

 * Simon H. Tausch <Simon.Tausch (at) bfr.bund.de> 

